#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <string>

#define FULL_TYPE_MESSAGE

#include <Engine.h>

// Function prototypes
void Start();
void OnEvent(Event e);
void Update();
void Close();

struct logger* AppLog;
struct logger* EngineLog;

double start = 0.0, end = 0.0;
double dt = 1.0;
double times = 0.0f;

int fps = 0;

CREATE_LOGGER(EngineLogV, EngineLog)
CREATE_LOGGER(AppLogV, AppLog)

#define LOG_E_INFO(...) EngineLogV(INFO, __VA_ARGS__)
#define LOG_E_WARN(...) EngineLogV(WARN, __VA_ARGS__)
#define LOG_E_CRIT(...) EngineLogV(CRIT, __VA_ARGS__)

#define LOG_A_INFO(...) AppLogV(INFO, __VA_ARGS__)
#define LOG_A_WARN(...) AppLogV(WARN, __VA_ARGS__)
#define LOG_A_CRIT(...) AppLogV(CRIT, __VA_ARGS__)

void InitEngine()
{
	// -- Initialized events --
	OnStart = Start;
	OnUpdate = Update;
	OnClose = Close;

	// App Event
	app->GetWindow()->OnEvent = &OnEvent;
	// --	--

	MALLO_OBJ(AppLog, logger, 1);
	AppLog->name = { "App" };
	AppLog->format = new char[18]{ "[%m:%s:%e]%n: %v\n" };


	MALLO_OBJ(EngineLog, logger, 1);
	EngineLog->name = { "Engine" };
	EngineLog->format = new char[18]{ "[%m-%s-%e]%n: %v\n" };

	LogLoggersInit(AppLog);
	LogLoggersInit(EngineLog);
}

void Start()
{
	LOG_E_INFO("Start");
}


void OnEvent(Event e)
{
	if (e.type == TypeEvent::WindowResize)
	{
		WindowResizeEvent* ev = (WindowResizeEvent*)e.event;

		int w = ev->w;
		int h = ev->h;

		Log(INFO, "Window size %ix%i", w, h);
	}

	if (e.type == TypeEvent::WindowFocuse)
	{
		WindowFocuseEvent ev = *(WindowFocuseEvent*)e.event;
		Log(CRIT, "Window Focuse %i", ev.focused);
	}

	if (e.type == TypeEvent::MouseScrolled)
	{
		MouseScrolledEvent ev = *(MouseScrolledEvent*)e.event;
		Log(INFO, "MouseScrolled %f %f", ev.x, ev.y);
	}

	if (e.type == TypeEvent::WindowClose)
	{
		Log(INFO, "Close window");
	}

	if (e.type == TypeEvent::KeyPressed)
	{
		KeyEvent ev = *(KeyEvent*)e.event;
		Log(INFO, "Key pressed %i", ev.keycode, ev.action);
	}
}

void Update()
{
	dt = (glfwgetTime() - start);
	start = glfwgetTime();

	UpdateInput(dt);

	//*
	if (GetKey(Key::Q))
	{
		//auto MousePos = GetMouseScroll();
		LOG_E_INFO("Mose pos %f %f ", GetScrollX(), GetScrollY());
	}
	/**/


	if (GetKey(Key::E) && GetKey(Key::Left_Alt))
	{
		app->Close();
	}

	if (GetKey(Key::A))
	{
		Log(INFO, "Memusage: %i", GetMemoryUsage());
	}


	times += dt;
	fps++;

	if (times >= 1.0f)
	{
		std::string title = "FPS: " + std::to_string(fps);
		app->SetTitle(title.c_str());

		LOG_E_INFO("FPS %i %f", fps, dt);

		times = 0.0f;
		fps = 0;
	}

}

void Close()
{
}

Application* CreateApplication()
{
	return ApplicationInit("Window", 800, 600);
}