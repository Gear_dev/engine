#include "WWindow.h"
#include "Engine/input/Input.h"

#ifdef ENGINE_DEBUG
#define ASSERT_RETURN_NULL(x, msg) \
	ENGINE_ASSERT(x, msg)
#else
#define ASSERT_RETURN_NULL(x, msg) \
if(!(x)) \
{ \
	Log(CRIT, msg); \
	return NULL; \
}
#endif

static void GLFWErrorCallback(int error, const char* description)
{
	Log(CRIT, "GLFW Error %i: %s", error, description);
}

// Window Event

void WindowPosCallback(GLFWwindow* window, int x, int y)
{
	Window* data = glfwGetWindowUserPointer(window);

	data->posX = x;
	data->posY = y;

	Event e = {
		WindowMoved,
		&((WindowMovedEvent) { x, y })
	};
	data->OnEvent(e);
}

void WindowSizeCallback(GLFWwindow* window, int w, int h)
{
	Window* data = glfwGetWindowUserPointer(window);

	data->width = w;
	data->height = h;

	Event e = {
		WindowResize,
		&((WindowResizeEvent) { w, h })
	};
	data->OnEvent(e);
}

void WindowFocusCallback(GLFWwindow* window, int focused)
{
	Window* data = glfwGetWindowUserPointer(window);

	Event e = {
		WindowFocuse,
		&((WindowFocuseEvent) { focused })
	};
	data->OnEvent(e);
}

void WindowCloseCallback(GLFWwindow* window)
{
	Window* data = glfwGetWindowUserPointer(window);

	WindowCloseEvent ev;
	Event e = {
		WindowClose,
		 &ev
	};
	data->OnEvent(e);

	data->IsRunning = 0;
	glfwDestroyWindow(window);
}

void WindowMaximizeCallback(GLFWwindow* window, int maximization)
{
	Window* data = glfwGetWindowUserPointer(window);
	data->IsMaximize = maximization;

	Event e = {
		WindowMaximize,
		&((WindowMaximizeEvent) { maximization })
	};
	data->OnEvent(e);
}

// Mouse Event

void MouseScrollCallback(GLFWwindow* window, double x, double y)
{
	Window* data = glfwGetWindowUserPointer(window);

	mouse.ScrollX = x;
	mouse.ScrollY = y;

	Event e = {
		MouseScrolled,
		&((MouseScrolledEvent) { x, y })
	};
	data->OnEvent(e);
}

void CursorPosCallback(GLFWwindow* window, double x, double y)
{
	Window* data = glfwGetWindowUserPointer(window);

	mouse.PosX = x;
	mouse.PosY = y;

	Event e = {
		MouseMoved,
		&((MouseMovedEvent) { x, y })
	};
	data->OnEvent(e);
}

void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	Window* data = glfwGetWindowUserPointer(window);

	switch(action)
	{
		case GLFW_PRESS:
		{
			Event e = {
				MouseButtonPressed,
				&((MouseButtonEvent) { button })
			};
			data->OnEvent(e);
			break;
		}
		case GLFW_RELEASE:
		{
			Event e = {
				MouseButtonReleased,
				&((MouseButtonEvent) { button })
			};
			data->OnEvent(e);
			break;
		}
	}
}

// Input Event

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	Window* data = glfwGetWindowUserPointer(window);

	switch(action)
	{
		case GLFW_RELEASE:
		{
			Event e = {
				KeyReleased,
				&((KeyEvent) { key, 0, mode })
			};
			data->OnEvent(e);
		}
		break;
		case GLFW_PRESS:
		{
			Event e = {
				KeyPressed,
				(&((KeyEvent) { key, 1, mode }))
			};
			data->OnEvent(e);
		}
		break;
		case GLFW_REPEAT:
		{
			Event e = {
				KeyPressed,
				(&((KeyEvent) { key, 2, mode }))
			};
			data->OnEvent(e);
		}
		break;
	}
}

void CharCallback(GLFWwindow* window, unsigned codepoint)
{
	Window* data = glfwGetWindowUserPointer(window);

	Event e = {
		KeyTyped,
		&((KeyTypedEvent) { codepoint })
	};
	data->OnEvent(e);
}


API GLFWwindow* CreateGLFWWindow(const char* title, unsigned w, unsigned h)
{
	int res = glfwInit();
	ASSERT_RETURN_NULL(res, "Failed to initialize GLFW");

	GLFWwindow* window = glfwCreateWindow(w, h, title, NULL, NULL);
	ASSERT_RETURN_NULL(window, "Failed to create GLFW window");

	glfwMakeContextCurrent(window);

	// glad init
	int glad = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	ASSERT_RETURN_NULL(glad, "Failed to initialize OpenGL context");

	// GLFW callbacks
	glfwSetErrorCallback(GLFWErrorCallback);

	// Window callback
	glfwSetWindowPosCallback(window, WindowPosCallback);
	glfwSetWindowSizeCallback(window, WindowSizeCallback);
	glfwSetWindowFocusCallback(window, WindowFocusCallback);
	glfwSetWindowCloseCallback(window, WindowCloseCallback);
	glfwSetWindowMaximizeCallback(window, WindowMaximizeCallback);

	// Mouse callback
	glfwSetScrollCallback(window, MouseScrollCallback);
	glfwSetCursorPosCallback(window, CursorPosCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);

	// keyboard callback
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCharCallback(window, CharCallback);

	return window;
}

void Close_Window(Window* window)
{
	WindowCloseEvent ev;
	Event e = {
		WindowClose,
		 &ev
	};
	window->OnEvent(e);

	window->IsRunning = 0;
	glfwSetWindowShouldClose(window->NativWindow, GLFW_FALSE);
}

API Window* CreateWWindow(const char* title, unsigned w, unsigned h)
{
	Window* wind = malloc(sizeof(Window));
	if(wind == NULL)
	{
		ENGINE_ASSERT(wind, "Failed to malloc for Window");
		wind->IsRunning = 0;
		wind->IsMaximize = 0;
		return NULL;
	}

	wind->NativWindow = (void*)CreateGLFWWindow(title, w, h);

	if(wind->NativWindow == NULL)
	{
		wind->IsRunning = 0;
		wind->IsMaximize = 0;
		return NULL;
	}

	wind->width = w;
	wind->height = h;
	wind->title = malloc(strlen(title) + 1);
	if (wind->title != NULL)
		strcpy(wind->title, title);

	glfwSetWindowUserPointer(wind->NativWindow, wind);

	glfwGetWindowPos(wind->NativWindow, &wind->posX, &wind->posY);
	wind->IsMaximize = glfwGetWindowAttrib(wind->NativWindow, GLFW_MAXIMIZED);

	wind->Close = &Close_Window;

	glViewport(0, 0, w, h);

	wind->IsRunning = 1;

	return wind;
}
