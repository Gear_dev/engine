#ifndef WWINDOW_H
#define WWINDOW_H

#include <Glad/glad.h>
#include <GLFW/glfw3.h>

#include "Engine/Window.h"

#include "Engine/log/Log.h"


API GLFWwindow* CreateGLFWWindow(const char* title, unsigned w, unsigned h);

API Window* CreateWWindow(const char* title, unsigned w, unsigned h);

#endif