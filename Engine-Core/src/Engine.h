#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include "Engine/Application.h"
#include "Engine/log/Log.h"
#include "Engine/utils/StringUtls.h"
#include "Engine/utils/memory.h"

#include "Engine/utils/Time.h"
#include "Engine/input/Input.h"

#include "Engine/EntryPoint.h"

#if defined(__cplusplus)
}
#endif
