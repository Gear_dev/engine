#ifndef WINDOW_H
#define WINDOW_H

#include "Core.h"

#include "Engine/events/Event.h"
#include "Engine/events/InputEvent.h"
#include "Engine/events/WindowEvent.h"


typedef struct _Window
{
	char* title;
	unsigned width, height;

	int posX, posY;

	int IsRunning;
	int IsMaximize;

	void(*NativWindow);

	void (*OnEvent)(Event e);

	void (*Close)(struct _Window* window);
	void (*OnStart)();
	void (*OnUpdate)();
	void (*OnClose)();
} Window;

#endif