#ifndef APPLICATION_H
#define APPLICATION_H

#include "Core.h"
#include "Window.h"

typedef struct _Application
{
	void (*OnStart)();
	void (*OnUpdate)();
	void (*OnClose)();

	void (*OnEvent)(Event e);

	void (*Close)();

	void (*SetTitle)(const char* title);
	void (*SetSize)(const int w, const int h);
	char* (*GetTitle)();
	const int (*GetWidht)();
	const int (*GetHeight)();

	const void (*SetPos)(int x, int y);
	const int (*GetPosX)();
	const int (*GetPosY)();

	Window* (*GetWindow)();
} Application;

API Application* ApplicationInit(const char* title, int w, int h);

API void ApplicationRun();

API double glfwgetTime();

#endif