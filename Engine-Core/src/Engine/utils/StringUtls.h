#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string.h>
#include <malloc.h>

#include "Engine/Core.h"

API inline size_t zstrlen(const char* str);

API int subStrCount(const char* str, const char* a);

API char* strAppend(const char* a, const char* b);

API inline char* getPtrChar(const char* text, const char symbol);

API int getCharIndex(const char* text, const char symbol);

API inline char* getPtrString(const char* str1, const char* str2);

API int getStringIndex(const char* text, const char* string);

API int stringContains(const char* text, const  char* find);

API void replaceChar(char* text, const char findSymbol, const char replace);

API char* replaceString(const char* str, const char* a, const char* b);

API char** splitString(const char* str, const char* delimiter, int* outCount);

#endif