#include "memory.h"
#include <malloc.h>

size_t mem_usage = 0;

inline void AddMem(size_t size)
{
	mem_usage += size;
}

inline void SubMem(size_t size)
{
	mem_usage -= size;
}

API void* engine_malloc(size_t size)
{
	void* tmp = malloc(size);
	AddMem(size);
	return tmp;
}

API void* engine_relloc(void* ptr, size_t old_size, size_t new_size)
{
	SubMem(old_size);
	AddMem(new_size);

	void* ptr_t = realloc(ptr, new_size);

	if (ptr_t != NULL)
		ptr = ptr_t;

	return ptr;
}

API void engine_free(void* v, size_t size)
{
	SubMem(size);
	free(v);
	v = NULL;
}

API inline size_t GetMemoryUsage()
{
	return mem_usage;
}
