#include "StringUtls.h"
#include "memory.h"

API inline size_t zstrlen(const char* str)
{
	return strlen(str) + 1;
}

API int subStrCount(const char* str, const char* a)
{
	if (str == NULL || a == NULL) return 0;

	int res = 0;
	int len = strlen(a);

	while (strstr(str, a) != NULL)
	{
		res++;
		if (str == NULL) break;
		str = strstr(str, a) + len;
	}

	return res;
}

API char* strAppend(const char* a, const char* b)
{
	size_t len = (strlen(a) + strlen(b));
	char* newStr;
	MALLO_OBJ(newStr, char, len + 1);
	if (newStr == NULL) return NULL;

	strcpy(newStr, a);
	strcpy(newStr + strlen(a), b);

	return newStr;
}

API inline char* getPtrChar(const char* text, const char symbol)
{
	return strchr(text, (int)symbol);
}

API int getCharIndex(const char* str1, const char ch)
{
	if (str1 == NULL) return -1;

	char* strP = getPtrChar(str1, ch);
	if (strP == NULL) return -1;

	return (strP - str1);
}

API inline char* getPtrString(const char* str1, const char* str2)
{
	return strstr(str1, str2);
}

API int getStringIndex(const char* str1, const char* str2)
{
	if (str1 == NULL || str2 == NULL) return -1;

	char* strP = getPtrString(str1, str2);
	if (strP == NULL) return -1;

	return (strP - str1);
}

API int stringContains(const char* text, const char* find)
{
	return getPtrString(text, find) != NULL;
}

API void replaceChar(char* text, const char findSymbol, const char replace)
{
	char* ptr = text;
	while (*ptr)
	{
		if (*ptr == findSymbol)
		{
			*ptr = replace;
		}
		++ptr;
	}
}

API char* replaceString(const char* str, const char* a, const char* b)
{
	if (strstr(str, a) == NULL) return NULL;

	int len = strlen(str);
	int lena = strlen(a);
	int lenb = strlen(b);

	int count = subStrCount((char*)str, a);
	int nsize = (len + (count * lenb - count * lena)) + 1;

	char* nstr;
	MALLO_OBJ(nstr, char, nsize);
	if (nstr == NULL) return NULL;

	memmove(nstr, str, len);

	for (char* ns = strstr(nstr, a), *p = strstr((char*)str, a) + lena; ns != NULL && p != NULL && strstr(ns, a) != NULL; p = strstr(p, a) + lena)
	{
		if (p == NULL)
		{
			FREE_OBJ(nstr, char, nsize);
			return NULL;
		}

		ns = (char*)memcpy(ns, b, lenb) + lenb;
		memcpy(ns, p, strlen(p));
		ns = strstr(ns, a);
	}
	nstr[nsize - 1] = '\0';

	return nstr;
}

API char** splitString(const char* str, const char* delimiter, int* outCount)
{
	int count = subStrCount(str, delimiter) + 1;
	*outCount = count;

	char* tmpStr = malloc(sizeof(char) * zstrlen(str));
	if (tmpStr == NULL) return NULL;
	strcpy(tmpStr, str);

	int i = 0;
	int size = 0;
	int memSize = sizeof(void*) * count;

	char** data = malloc(memSize);
	if (data == NULL) return NULL;

	memset(data, 0, memSize);

	char* p;
	p = strtok(tmpStr, delimiter);

	size = zstrlen(p);
	data[i] = malloc(size);

	if (data[i] == NULL) return NULL;
	strcpy(data[i], p);
	i++;

	do {
		p = strtok('\0', delimiter);
		if (p)
		{
			size = zstrlen(p);

			data[i] = malloc(size);

			if (data[i] == NULL)
			{
				for (int c = 0; c < i; c++)
				{
					free(data[c]);
				}
				free(data);
				*outCount = 0;
				return NULL;
			}

			strcpy(data[i], p);

			i++;
		}
	} while (p && i < count);
	memset(tmpStr, 0, strlen(str) - 1);
	free(tmpStr);


	return data;
}
