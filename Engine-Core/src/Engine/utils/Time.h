#ifndef TIME_H
#define TIME_H

#include <Windows.h>

#include "Engine/Core.h"
#include "Engine/utils/memory.h"

typedef struct _Time
{
	unsigned short year;
	unsigned short month;
	unsigned short dayOfWeek;
	unsigned short day;
	unsigned short hour;
	unsigned short minute;
	unsigned short second;
	unsigned short mseconds;

} Time;

#define FORMAT_NAME_SIZE 8
static const char* FormatName[FORMAT_NAME_SIZE] = { "%Y", "%M", "%W", "%D", "%h", "%m", "%s", "%e" };

API int GetSizeFormatChar(const char ch);

API int GetCountFormatSymbol(const char* format);

API int GetSizeFormatSymbol(const char* format);

API void GetTime(Time* time);

API unsigned short* GetTimeData(const Time* time);

API unsigned short GetTimeElementByFormat(const Time* time, const char* name);

API unsigned short* GetTimeElementsByFormat(const Time* time, const char* format, int* outCount);

API char* GetTimeStr(Time* time, const char* format);

#endif