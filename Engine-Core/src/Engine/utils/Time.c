#include "Time.h"
#include "StringUtls.h"

#include <stdio.h>
#include <stdlib.h>

int IsCharCmpFormat(const char ch)
{
	for (int i = 0; i < FORMAT_NAME_SIZE; i++)
		if (ch == FormatName[i][1])
			return 1;
	return 0;
}

int GetIndexByCharCmpFormat(const char ch)
{
	for (int i = 0; i < FORMAT_NAME_SIZE; i++)
		if (ch == FormatName[i][1])
			return i;
	return -1;
}

API int GetSizeFormatChar(const char ch)
{
	switch (ch)
	{
	case 'Y': return 4;
	case 'M': return 2;
	case 'W': return 2;
	case 'D': return 2;
	case 'h': return 2;
	case 'm': return 2;
	case 's': return 2;
	case 'e': return 3;

	default: return 0;
	}
}

inline unsigned short GetTimeByFormat(const Time* time, const char* format)
{
	unsigned short* gt = GetTimeData(time);

	for (int i = 0; i < FORMAT_NAME_SIZE; i++)
		if (strstr(format, FormatName[i]) != NULL)
			return gt[i];
	return 0;
}

API int GetCountFormatSymbol(const char* format)
{
	int res = 0;
	const char* ptr = &format[0];

	char* p = getPtrChar(format, '%');
	if (p == NULL) return 0;

	do
	{
		if (p != NULL && IsCharCmpFormat(*(++p)))
			res++;
		p = getPtrChar(p, '%');
	} while (p);

	return res;
}

API int GetSizeFormatSymbol(const char* format)
{
	int res = 0;
	const char* ptr = &format[0];

	for (int i = 0; i < FORMAT_NAME_SIZE; i++)
	{
		int index = getStringIndex(ptr, FormatName[i]);

		while (index >= 0)
		{
			index = getStringIndex(ptr, FormatName[i]);
			if (index == -1) break;
			res++;

			ptr += strlen(FormatName[i]);
			if (*ptr != '%') ptr++;
		}
	}

	return res;
}

API void GetTime(Time* time)
{
#if defined(_WIN32)
	SYSTEMTIME tm = *(SYSTEMTIME*)(void*)time;
	GetLocalTime(&tm);
	memcpy(time, &tm, sizeof(Time));
#endif
}

API unsigned short* GetTimeData(const Time* time)
{
	return (unsigned short*)time;
}

API unsigned short GetTimeElementByFormat(const Time* time, const char* name)
{
	return GetTimeByFormat(time, name);
}

API unsigned short* GetTimeElementsByFormat(const Time* time, const char* format, int* outCount)
{
	size_t count = GetCountFormatSymbol(format);
	unsigned short* gt = GetTimeData(time);
	*outCount = count;

	unsigned short* data;
	MALLO_OBJ(data, unsigned short, count);

	char* ptr = getPtrChar(format, '%');
	if (ptr == NULL)
	{
		FREE_OBJ(data, unsigned short, count);
		*outCount = 0;
		return NULL;
	}

	size_t c = 0;
	do
	{
		if (ptr != NULL && IsCharCmpFormat(*(++ptr)))
		{
			int i = GetIndexByCharCmpFormat(*ptr);
			data[c] = gt[i];
			c++;
		}
		ptr = getPtrChar(ptr, '%');

	} while (c < count && ptr);

	return data;
}

API char* GetTimeStr(Time* time, const char* format)
{
	int count = 0;
	unsigned short* gt = GetTimeElementsByFormat(time, format, &count);

	int stlen = strlen(format);
	int size = sizeof(char) * (stlen + (count * 3)) + 1;

	char* data;
	MALLO_OBJ(data, char, size);
	if (data == NULL) return NULL;
	memset(data, 0, size);
	strcpy(data, format);


	char* ptr = getPtrChar(data, '%');
	if (ptr == NULL)
		return NULL;

	int c = 0;
	do
	{
		if (ptr != NULL && IsCharCmpFormat(*(++ptr)))
		{
			char* tmp = ++ptr;
			int sizeFm = GetSizeFormatChar(*(--ptr)) + 1;

			char* tstr;

			tstr = malloc(strlen(tmp) + 1);
			if (tstr == NULL)
			{
				free(data);
				return NULL;
			}
			strcpy(tstr, tmp);

			char* buffer = malloc(sizeof(char) * sizeFm);
			if (buffer == NULL)
			{
				free(data);
				return NULL;
			}

			if (sizeFm <= 3)
				sprintf(buffer, "%.2i", gt[c]);
			else
				sprintf(buffer, "%.3i", gt[c]);

			ptr--;

			memmove(ptr, buffer, sizeFm - 1);
			if (sizeFm > 3)
			{
				ptr += sizeFm - 1;
				strcpy(ptr, tstr);
			}

			free(tstr);
			free(buffer);

			c++;
		}

		ptr = getPtrChar(ptr, '%');
	} while (ptr && c < count);

	FREE_OBJ(gt, unsigned int, count);

	return data;
}
