#ifndef MEMORY_H
#define MEMORY_H

#include "Engine/Core.h"
#include <stdio.h>
#include <string.h>

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

//#define ENGINE_DEBUG_LOG
#ifdef ENGINE_DEBUG_LOG

#define MALLO_OBJ(var, type, size) \
{																				\
	size_t sz = sizeof(type) * (size);											\
	printf("new %s size %i | %s(%i) \n", #var, sz, __FILENAME__, __LINE__);		\
	var = (type *)engine_malloc(sz);											\
}

#define RELLOC_OBJ(var, type, osize, nsize) \
{																				\
	size_t sz = sizeof(type) * (nsize);											\
	printf("relloc %s (old size %i) size %i | %s(%i) \n", #var, osize, sz, __FILENAME__, __LINE__);	\
	size_t sz = sizeof(type) * (nsize);		\
	var = engine_relloc(var, osize, sz);	\
}

#define FREE_OBJ(var, type, size) \
{																				\
	size_t sz = sizeof(type) * (size);											\
	printf("free %s size %i | %s(%i) \n", #var, sz, __FILENAME__, __LINE__);	\
	engine_free(var, sz);														\
	var = NULL;																	\
}

#else

#define MALLO_OBJ(var, type, size) \
{										\
	size_t sz = sizeof(type) * (size);	\
	var = (type *)engine_malloc(sz);	\
}

#define RELLOC_OBJ(var, type, osize, nsize) \
{											\
	size_t sz = sizeof(type) * (nsize);		\
	var = engine_relloc(var, osize, sz);	\
}

#define FREE_OBJ(var, type, size) \
{										\
	size_t sz = sizeof(type) * (size);	\
	engine_free(var, sz);				\
	var = NULL;							\
}

#endif


API void* engine_malloc(size_t size);
API void* engine_relloc(void* ptr, size_t old_size, size_t new_size);
API void  engine_free(void* v, size_t size);

API inline size_t GetMemoryUsage();

#endif // MEMORY_H