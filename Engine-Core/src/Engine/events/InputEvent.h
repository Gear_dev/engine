#ifndef INPUT_EVENT_H
#define INPUT_EVENT_H

#include "Event.h"


typedef struct _MouseScrolledEvent
{
	const double x;
	const double y;
} MouseScrolledEvent;

typedef struct _MouseMovedEvent
{
	const double x;
	const double y;
} MouseMovedEvent;

typedef struct _MouseButtonEvent
{
	const int button;
} MouseButtonEvent;


typedef struct _KeyEvent
{
	const int keycode;
	const int action;
	const int mode;
} KeyEvent;

typedef struct _KeyTypedEvent
{
	const unsigned code;
} KeyTypedEvent;

#endif