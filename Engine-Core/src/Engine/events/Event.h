#ifndef EVENT_H
#define EVENT_H

typedef enum _TypeEvent
{
	None = 0,

	WindowClose,
	WindowMoved,
	WindowFocuse,
	WindowResize,
	WindowMaximize,

	MouseButtonPressed,
	MouseButtonReleased,
	MouseScrolled,
	MouseMoved,

	KeyPressed,
	KeyReleased,
	KeyTyped

} TypeEvent;

typedef struct _Event
{
	const TypeEvent type;
	const void* event;
} Event;


#endif // EVENT_H