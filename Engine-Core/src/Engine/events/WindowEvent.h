#ifndef WINDOW_EVENT_H
#define WINDOW_EVENT_H

#include "Event.h"


typedef struct _WindowMovedEvent
{
	const int x;
	const int y;
} WindowMovedEvent;

typedef struct _WindowResizeEvent
{
	const int w;
	const int h;
} WindowResizeEvent;

typedef struct _WindowFocuseEvent
{
	const int focused;
} WindowFocuseEvent;

typedef struct _WindowCloseEvent
{
	int : 0;
} WindowCloseEvent;

typedef struct _WindowMaximizeEvent
{
	const int maximization;
} WindowMaximizeEvent;

#endif