#ifndef INPUT_H
#define INPUT_H

#include <Windows.h>
#include "key.h"

#include "Engine/Core.h"
#include "Engine/Application.h"

#ifdef ENGINE_DLL
struct Mouse {
	double PosX;
	double PosY;

	double ScrollX;
	double ScrollY;
} mouse;
#endif

API void InitInput(Application* app);
API void UpdateInput(double dt);

API int GetKey(int key);
API int GetKeyUp(int key);
API int GetKeyDown(int key);

API int GetButton(int button);
API int GetButtonUp(int button);
API int GetButtonDown(int button);

API double GetMousePosX();
API double GetMousePosY();
API double* GetMousePos();

API double GetScrollX();
API double GetScrollY();
API double* GetMouseScroll();

#endif