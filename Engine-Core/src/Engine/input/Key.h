#ifndef KEY_H
#define KEY_H

typedef enum _Key
{
	Key_Unknown = 0,

	Space = 32,
	Apostrophe = 39,  /* ' */
	Comma = 44,  /* , */
	Minus = 45,  /* - */
	Period = 46,  /* . */
	Slash = 47,  /* / */
	Key0 = 48,
	Key1 = 49,
	Key2 = 50,
	Key3 = 51,
	Key4 = 52,
	Key5 = 53,
	Key6 = 54,
	Key7 = 55,
	Key8 = 56,
	Key9 = 57,
	Semicolon = 59,  /* ; */
	Equal = 61,  /* = */
	A = 65,
	B = 66,
	C = 67,
	D = 68,
	E = 69,
	F = 70,
	G = 71,
	H = 72,
	I = 73,
	J = 74,
	K = 75,
	L = 76,
	M = 77,
	N = 78,
	O = 79,
	P = 80,
	Q = 81,
	R = 82,
	S = 83,
	T = 84,
	U = 85,
	V = 86,
	W = 87,
	X = 88,
	Y = 89,
	Z = 90,
	Left_Bracket = 91,  /* [ */
	Backslash = 92,  /* \ */
	Right_Bracket = 93,  /* ] */
	Grave_Accent = 96,  /* ` */
	World_1 = 161, /* non-US #1 */
	World_2 = 162, /* non-US #2 */

	Key_Escape = 256,
	Enter = 257,
	Tab = 258,
	Backspace = 259,
	Insert = 260,
	Delete = 261,
	Right = 262,
	Left = 263,
	Down = 264,
	Up = 265,
	Page_Up = 266,
	Page_Down = 267,
	Home = 268,
	End = 269,
	Caps_Lock = 280,
	Scroll_Lock = 281,
	Num_Lock = 282,
	Print_Screen = 283,
	Pause = 284,
	F1 = 290,
	F2 = 291,
	F3 = 292,
	F4 = 293,
	F5 = 294,
	F6 = 295,
	F7 = 296,
	F8 = 297,
	F9 = 298,
	F10 = 299,
	F11 = 300,
	F12 = 301,
	F13 = 302,
	F14 = 303,
	F15 = 304,
	F16 = 305,
	F17 = 306,
	F18 = 307,
	F19 = 308,
	F20 = 309,
	F21 = 310,
	F22 = 311,
	F23 = 312,
	F24 = 313,
	F25 = 314,
	KP_0 = 320,
	KP_1 = 321,
	KP_2 = 322,
	KP_3 = 323,
	KP_4 = 324,
	KP_5 = 325,
	KP_6 = 326,
	KP_7 = 327,
	KP_8 = 328,
	KP_9 = 329,
	KP_Decimal = 330,
	KP_Divide = 331,
	KP_Multiply = 332,
	KP_Subtract = 333,
	KP_Add = 334,
	KP_Enter = 335,
	KP_Equal = 336,
	Left_Shift = 340,
	Left_Control = 341,
	Left_Alt = 342,
	Left_Super = 343,
	Right_Shift = 344,
	Right_Control = 345,
	Right_Alt = 346,
	Right_Super = 347,
	Menu = 348,
	Key_Last = Menu
} Key;

typedef enum _KeyMode
{
	KeyMode_Shift = 0x0001,
	KeyMode_Control = 0x0002,
	KeyMode_Alt = 0x0004,
	KeyMode_Super = 0x0008,
	KeyMode_Caps_Lock = 0x0010,
	KeyMode_Num_Lock = 0x0020
} KeyMode;


typedef enum _MouseButton
{
	B_1 = 0,
	B_2 = 1,
	B_3 = 2,
	B_4 = 3,
	B_5 = 4,
	B_6 = 5,
	B_7 = 6,
	B_8 = 7,
	MouseButton_Last = B_8,
	MouseButton_Left = B_1,
	MouseButton_Right = B_2,
	MouseButton_Middle = B_3
} MouseButton;


typedef enum _Joystick
{
	Joystick_1 = 0,
	Joystick_2 = 1,
	Joystick_3 = 2,
	Joystick_4 = 3,
	Joystick_5 = 4,
	Joystick_6 = 5,
	Joystick_7 = 6,
	Joystick_8 = 7,
	Joystick_9 = 8,
	Joystick_10 = 9,
	Joystick_11 = 10,
	Joystick_12 = 11,
	Joystick_13 = 12,
	Joystick_14 = 13,
	Joystick_15 = 14,
	Joystick_16 = 15,
	Joystick_Last = Joystick_16
} Joystick;

typedef enum _GamepadButtons
{
	Button_A = 0,
	Button_B = 1,
	Button_X = 2,
	Button_Y = 3,
	Button_Left_Bumper = 4,
	Button_Right_Bumper = 5,
	Button_Back = 6,
	Button_Start = 7,
	Button_Guide = 8,
	Button_Left_Thumb = 9,
	Button_Right_Thumb = 10,
	Button_Dpad_Up = 11,
	Button_Dpad_Right = 12,
	Button_Dpad_Down = 13,
	Button_Dpad_Left = 14,
	Button_LAST = Button_Dpad_Left,

	Button_Cross = Button_A,
	Button_Circle = Button_B,
	Button_Square = Button_X,
	Button_Triangle = Button_Y
} GamepadButtons;

typedef enum _GamepadAxis
{
	Left_X = 0,
	Left_Y = 1,
	Right_X = 2,
	Right_Y = 3,
	Left_Trigger = 4,
	Right_Trigger = 5,
	Last = Right_Trigger
} GamepadAxis;

#endif