#include "Input.h"

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <stdio.h>

Application* app;
#define WINDOW ((GLFWwindow*)app->GetWindow()->NativWindow)

int Keys[Key_Last];
int Buttons[MouseButton_Last];

double times = 0.0;

double lastScrollX = 0.0;
double lastScrollY = 0.0;


API void InitInput(Application* _app)
{
	app = _app;
}

API void UpdateInput(double dt)
{
	times += dt;

	if (times > 0.1)
	{
		times = 0.0;

		if (lastScrollX == mouse.ScrollX) mouse.ScrollX = 0.0;
		if (lastScrollY == mouse.ScrollY) mouse.ScrollY = 0.0;
	}

	lastScrollX = mouse.ScrollX;
	lastScrollY = mouse.ScrollY;
}


API int GetKey(int key)
{
	GLFWwindow* window = WINDOW;
	int state = glfwGetKey(window, key);
	return state == GLFW_PRESS || state == GLFW_REPEAT;
}

API int GetKeyUp(int key)
{
	GLFWwindow* window = WINDOW;
	int state = glfwGetKey(window, key);
	Keys[key] = 0;
	return state == GLFW_RELEASE;
}

API int GetKeyDown(int key)
{
	int state = GetKey(key);

	if (!state) Keys[key] = 0;
	if (state && !Keys[key])
	{
		Keys[key] = 1;
		return 1;
	}
	return 0;
}


API int GetButton(int button)
{
	GLFWwindow* window = WINDOW;
	int state = glfwGetMouseButton(window, button);
	return state == GLFW_PRESS;
}

API int GetButtonUp(int button)
{
	GLFWwindow* window = WINDOW;
	int state = glfwGetMouseButton(window, button);
	Buttons[button] = 0;
	return state == GLFW_RELEASE;
}

API int GetButtonDown(int button)
{
	int state = GetButton(button);

	if (!state) Buttons[button] = 0;
	if (state && !Buttons[button])
	{
		Buttons[button] = 1;
		return 1;
	}
	return 0;
}


API double GetMousePosX()
{
	GLFWwindow* window = WINDOW;
	glfwGetCursorPos(window, &mouse.PosX, &mouse.PosY);
	return mouse.PosX;
}

API double GetMousePosY()
{
	GLFWwindow* window = WINDOW;
	glfwGetCursorPos(window, &mouse.PosX, &mouse.PosY);
	return mouse.PosY;
}

API double* GetMousePos()
{
	GLFWwindow* window = WINDOW;
	glfwGetCursorPos(window, &mouse.PosX, &mouse.PosY);
	return &mouse.PosX;
}


API double GetScrollX()
{
	return mouse.ScrollX;
}

API double GetScrollY()
{
	return mouse.ScrollY;
}

API double* GetMouseScroll()
{
	return &mouse.ScrollX;
}
