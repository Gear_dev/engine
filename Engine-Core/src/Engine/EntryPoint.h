#pragma once

#ifndef ENGINE_DLL

void InitEngine();

// -- base events --
void (*OnStart)() = NULL;
void (*OnUpdate)() = NULL;
void (*OnClose)() = NULL;
// --	--

extern Application* CreateApplication();

Application* app;

int main(int argc, char** argv)
{
	LogInit("[%m:%s:%e]%v \n");
	Log(INFO, "Initialized log system");

	app = CreateApplication();
	InitEngine();
	InitInput(app);

	app->GetWindow()->OnStart = OnStart;
	app->GetWindow()->OnUpdate = OnUpdate;
	app->GetWindow()->OnClose = OnClose;

	ApplicationRun();

	free(app);

	return 0;
}

#endif