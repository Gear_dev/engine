#include "Log.h"

Time _time;

#if defined(_WIN32)
HANDLE hConsole;
#endif

const char* _timeFormat = "[%m:%s:%e]%v\n";

int LogLoggersList(struct logger* _logger, enum TypeMessage type, const char* _Format, va_list list);

API void LogLoggersInit(struct logger* _logger)
{
	char* tmp = replaceString(_logger->format, "%n", _logger->name);
	free(_logger->format);
	_logger->format = tmp;

	_logger->Log = &LogLoggersList;
}

API void LogInit(const char* format)
{
	LogSetTimeFormat(format);

#if defined(_WIN32)
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#endif
}

API void LogSetTimeFormat(const char* format)
{
	_timeFormat = format;
}

API const char* LogGetTimeFormat()
{
	return _timeFormat;
}

void SetConsoleColor(enum TypeMessage type)
{
#if defined(_WIN32)
	switch (type)
	{
	case Information:
	case Warning:
	case Critical:
		SetConsoleTextAttribute(hConsole, type);
		break;
	default:
		SetConsoleTextAttribute(hConsole, 7); // white
		break;
	}
#endif
}

API int LogList(const enum TypeMessage type, const char* _Format, va_list list)
{
	GetTime(&_time);

	int _Result;

	char* aa = GetTimeStr(&_time, _timeFormat);
	char* outs = replaceString(aa, "%v", _Format);

	SetConsoleColor(type);

	_Result = vprintf(outs, list);

	FREE_OBJ(aa, char, zstrlen(aa));
	FREE_OBJ(outs, char, zstrlen(outs));

	SetConsoleColor(Information);

	return _Result;
}

API int Log(const enum TypeMessage type, const char* _Format, ...)
{
	int _Result;

	va_list args;
	va_start(args, _Format);
	_Result = LogList(type, _Format, args);
	va_end(args);

	return _Result;
}

int LogLoggersList(struct logger* _logger, enum TypeMessage type, const char* _Format, va_list list)
{
	GetTime(&_time);

	int _Result;

	struct logger* ptr = _logger;
	char* tm = GetTimeStr(&_time, ptr->format);
	char* outs = replaceString(tm, "%v", _Format);

	SetConsoleColor(type);

	_Result = vprintf(outs, list);

	SetConsoleColor(Information);

	FREE_OBJ(tm, char, zstrlen(tm));
	FREE_OBJ(outs, char, zstrlen(outs));

	return _Result;
}
