#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <Windows.h>

#include "Engine/Core.h"
#include "Engine/utils/StringUtls.h"
#include "Engine/utils/Time.h"

#define INFO Information
#define WARN Warning
#define CRIT Critical

#define LOG_INFO(text, ...) Log(INFO, text, __VA_ARGS__)
#define LOG_WARN(text, ...) Log(WARN, text, __VA_ARGS__)
#define LOG_CRIT(text, ...) Log(CRIT, text, __VA_ARGS__)

typedef enum _TypeMessage
{
	Information = 7,
	Warning = 6,
	Critical = 4
} TypeMessage;

#define CREATE_LOGGER(name, _logger) \
int name(TypeMessage type, const char* msg, ...) {			\
	va_list args;												\
	va_start(args, msg);										\
	int result = _logger->Log(_logger, type, msg, args);		\
	va_end(args);												\
	return result;												\
}

typedef int LogLogger(struct logger* _logger, TypeMessage type, const char* _Format, va_list list);

struct logger
{
	const char* name;
	char* format;

	LogLogger* Log;
};

API void LogLoggersInit(struct logger* _logger);

API void LogInit(const char* format);

API void LogSetTimeFormat(const char* format);

API const char* LogGetTimeFormat();

inline const char* LogGetTypeMessage(const TypeMessage type)
{
	switch (type)
	{
#ifdef FULL_TYPE_MESSAGE
	case Warning:		return "Warning";
	case Critical:		return "Critical";
	case Information:	return "Information";
#else
	case Information:	return "Info";
	case Warning:		return "Warn";
	case Critical:		return "Crit";
#endif

	default:			return "???";
	}
}

API int LogList(const TypeMessage type, const char* _Format, va_list list);

API int Log(const TypeMessage type, const char* _Format, ...);

#endif