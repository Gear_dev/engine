#ifndef CORE_H
#define CORE_H

#ifdef ENGINE_DLL
#define API __declspec(dllexport)
#else
#define API __declspec(dllimport)
#endif

#define BIT(x) (1 << x)
#define CHECK_BIT(var, pos) ((var) & BIT(pos))

#ifdef ENGINE_DEBUG
#define ENGINE_ASSERT(x, ...) { if(!(x)) { Log(CRIT, __VA_ARGS__); __debugbreak(); } }
#else
#define ENGINE_ASSERT(x, ...)
#endif

#endif