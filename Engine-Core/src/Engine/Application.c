#include "Application.h"
#include "Platform/Windows/WWindow.h"

#define GLFW_EXPOSE_NATIVE_WIN32
#include "GLFW/glfw3native.h"

API Window* window;

void AppClose()
{
	window->Close(window);
}

void AppSetTitle(const char* title);
char* AppGetTitle();
void AppSetSize(const int w, const int h);
const int AppGetWidht();
const int AppGetHeight();

const void AppSetPos(int x, int y);
const int AppGetPosX();
const int AppGetPosY();


Window* AppGetWindow()
{
	return window;
}

inline GLFWwindow* GLFWGetWindow()
{
	return window->NativWindow;
}


API Application* ApplicationInit(const char* title, int w, int h)
{
	window = CreateWWindow(title, w, h);

	Application* app = (Application*)malloc(sizeof(Application));

	if(window == NULL || app == NULL)
		return NULL;
	
	app->Close = &AppClose;

	app->SetTitle = &AppSetTitle;
	app->GetTitle = &AppGetTitle;

	app->SetSize = &AppSetSize;
	app->GetWidht = &AppGetWidht;
	app->GetHeight = &AppGetHeight;

	app->SetPos = &AppSetPos;
	app->GetPosX = &AppGetPosX;
	app->GetPosY = &AppGetPosY;

	app->GetWindow = &AppGetWindow;

	return app;
}

API void ApplicationRun()
{
	window->OnStart();

	GLFWwindow* wind = GLFWGetWindow();

	while(window->IsRunning)
	{
		window->OnUpdate();

		glfwSwapBuffers(wind);
		glfwPollEvents();
	}

	window->OnClose();

	glfwTerminate();

	free(window);
}

void AppSetTitle(const char* title)
{
	GLFWwindow* glfwWindow = GLFWGetWindow();

#ifndef _WIN32
	_window->title = title;
#endif
	glfwSetWindowTitle(glfwWindow, title);
}

char* AppGetTitle()
{
	GLFWwindow* glfwWindow = GLFWGetWindow();

#ifdef _WIN32
	HWND hwnd = glfwGetWin32Window(glfwWindow);

	int size = GetWindowTextLength(hwnd) + 1;

	if(window->title == NULL)
	{
		window->title = malloc(size);
	}
	else
	{
		free(window->title);
		window->title = malloc(size);
		ENGINE_ASSERT(window->title, ""__FILE__" Allocation error.");
	}

	GetWindowTextA(hwnd, window->title, size);
#endif

	return window->title;
}

void AppSetSize(const int w, const int h)
{
	GLFWwindow* glfwWindow = GLFWGetWindow();
	glfwSetWindowSize(glfwWindow, w, h);
}

const int AppGetWidht()
{
	return window->width;
}

const int AppGetHeight()
{
	return window->height;
}

const void AppSetPos(int x, int y)
{
	GLFWwindow* glfwWindow = GLFWGetWindow();
	glfwSetWindowPos(glfwWindow, x, y);
}

const int AppGetPosX()
{
	return window->posX;
}

const int AppGetPosY()
{
	return window->posY;
}

API double glfwgetTime()
{
	return glfwGetTime();
}

